# WHATs and WHYs
**Postgres** -> for the simplicity of data import from CSV and fool-proofness of its drivers across different languages. 
UUID (serial can be used too) is generated upon import.

**Golang** -> there's no other language like Golang ♥.
`gin/gonic` is also ♥, simple and effective (and performant) HTTP serving.
And well, `database/sql` is crude, but the standard of running SQL queries from an app.

---

Folder `db` contains Dockerfile for DB, init script (that runs import during startup) and data file.

Folder `app` contains Dockerfile (multi-stage golang + alpine) for app, alongside with sources.

# HOW-TO
*Prerequisites*: running Kubernetes cluster and configured `kubectl`
*In my case, it was just*:
```
gcloud container clusters create apitest --num-nodes=1 --zone=europe-west1-c
gcloud container clusters get-credentials apitest
```

For this project GKE was used since I just wanted to give Google Cloud a try and save some time setting up cluster.

*(to be honest, I currently use CFN + Ansible playbook + bash trickery for AWS EKS and it's ugly :))*

Run `deploy.sh <docker_image_tag>` which builds and pushes images, creates K8S namespace, DB secret (from file in `db` folder, to be improved by Vault or SSM or ...), applies the `kustomization` and deploys 3 replicas of `app` and an instance of `db` (just to avoid StatefulSets, hehe) (and some commented stuff).
Same `deploy.sh` with a newer image tag would update the deployment.

Since `LoadBalancer` type is used with `Service`, this creates L4 LB (HTTP) on GCE.

HTTPS-wise, I've included commented-out part in `deployment.yml`/`deploy.sh`, that would (pinkie-swear) do HTTPS on a domain (if I'd had one spare lying around - I would uncomment it).
It's actually sad there's no alternative to `AWS ALB with HTTPS + Ingress` at GKE. It's either NGINX ingress +  trickery with LetsEncrypt or solution I offered.

Anyway, `kubectl get service -n apitest app` will get us publicly available IP of a load balancer. All API calls need to be directed to port 8181.
See below.


List all passengers:
```
curl -XGET -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people
```
Result: `big-big JSON`


Get info of a passenger:
```
curl -XGET -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<uuid>
```
Result: `{"uuid":1,"survived":false,"pclass":3,"pname":"Mr. Owen Harris Braund","sex":"male","age":22,"ss_onboard":1,"pc_onboard":0,"fare":7.25}`

```
curl -XGET -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<non_existing_uuid>
```
Result: `"No user with uuid <non_existing_uuid> found"`


Update a passenger:
```
curl -XPUT -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<uuid> -d '{ "survived": true, "pclass": 3, "pname": "MrOwen", "sex": "male", "age": 22, "ssOnboard": 1, "pcOnboard":0, "fare":7}'
```
Result: `"Successfully updated user with uuid: <uuid>"`

```
curl -XPUT -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<non_existing_uuid> -d '{ "survived": true, "pclass": 3, "pname": "MrOwen", "sex": "male", "age": 22, "ssOnboard": 1, "pcOnboard":0, "fare":7}'
```
Result: `"There was nothing to update"`


Create a passenger:
```
curl -XPOST -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people -d '{ "survived": true, "pclass": 3, "pname": "MrOwen", "sex": "male", "age": 22, "ssOnboard": 1, "pcOnboard":0, "fare":7}'
```
Result: `"Added a passenger with uuid: <new-uuid>"`


Delete a passenger:
```
curl -XDELETE -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<non_existing_uuid>
```
Result: `"There was nothing to delete"`

```
curl -XDELETE -H "Content-Type: application/json" http://<public_ip_of_l4lb>:8181/people/<uuid>
```
Result: `"Successfully deleted user with uuid: <uuid>"`




# IMPROVEMENTS:
- Structured logging
- Better error handling
- More meaningful response messages
- ORM like https://github.com/go-pg/pg

===

# API-exercise

This exercise is to assess your technical proficiency with Software Engineering, DevOps and Infrastructure tasks.
There is no need to do all the exercises, but try to get as much done as you can, so we can get a good feel of your skillset.  Don't be afraid to step outside your comfort-zone and try something new.

If you have any questions, feel free to reach out to us.

## Exercise

This exercise is split in several subtasks. We are curious to see where you feel most comfortable and where you struggle.

### 0. Fork this repository
All your changes should be made in a **private** fork of this repository. When you're done please, please:
* Share your fork with the **container-solutions-test** user (Settings -> Members -> Share with Member)
* Make sure that you grant this user the Reporter role, so that our reviewers can check out the code using Git.
* Reply to the email that asked you to do this API exercise, with a link to the repository that the **container-solutions-test** user now should have access to.

### 1. Setup & fill database
In the root of this project you'll find a csv-file with passenger data from the Titanic. Create a database and fill it with the given data. SQL or NoSQL is your choice.

### 2. Create an API
Create an HTTP-API (e.g. REST) that allows reading & writing (maybe even updating & deleting) data from your database.
Tech stack and language are your choice. The API we would like you to implement is described in [API.md](./API.md)

### 3. Dockerize
Automate setup of your database & API with Docker, so it can be run everywhere comfortably with one or two commands.
The following build tools are acceptable:
 * docker
 * docker-compose
 * groovy
 * minikube (see 4.)

No elaborate makefiles please.

#### Hints

- [Docker Install](https://www.docker.com/get-started)

### 4. Deploy to Kubernetes
Enable your Docker containers to be deployed on a Kubernetes cluster.

#### Hints

- Don't have a Kubernetes cluster to test against?
  - [MiniKube](https://kubernetes.io/docs/setup/minikube/) (free, local)
  - [GKE](https://cloud.google.com/kubernetes-engine/) (more realistic deployment, may cost something)

### 5. Whatever you can think of
Do you have more ideas to optimize your workflow or automate deployment? Feel free to go wild and dazzle us with your solutions.
