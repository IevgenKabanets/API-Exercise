#!/bin/bash -xe
echo 'Building and publishing Docker images...'
docker build -f app/Dockerfile.app -t kabae/api-test-app:$1 app
docker push kabae/api-test-app:$1

docker build -f db/Dockerfile.db -t kabae/api-test-postgres:$1 db
docker push kabae/api-test-postgres:$1

# Uncomment to enable Google-managed certificate to be auto-generated for Ingress.
# curl https://raw.githubusercontent.com/GoogleCloudPlatform/gke-managed-certs/master/deploy/managedcertificates-crd.yaml | kubectl apply -f -
# curl https://raw.githubusercontent.com/GoogleCloudPlatform/gke-managed-certs/master/deploy/managed-certificate-controller.yaml | kubectl apply -f -

echo 'Creating namespace...'
if kubectl get namespace | grep -q apitest; then
    echo 'Namespace apitest already exists'
else
    kubectl create namespace apitest
fi

echo 'Creating secrets from super-secret file'
if kubectl get secret -n apitest | grep -q dbsecret; then
    echo 'Secret already exists'
else
    kubectl create secret -n apitest generic dbsecret --from-file=./db/.password.secret
fi

echo 'Running kustomize to edit image versions'
kustomize edit set image kabae/api-test-app:$1
kustomize edit set image kabae/api-test-postgres:$1

echo 'Deploying after all this struggle...'
kustomize build . | kubectl apply -f -