package main

import (
	"log"
	"os"

	"gitlab.com/IevgenKabanets/API-Exercise/router"
)

func main() {

	// set the hostname
	hostname, _ := os.Hostname()

	// create the REST api server
	apiRouter := router.ApiEngine()

	// run the api router
	err := apiRouter.Run("0.0.0.0:8181")
	if err != nil {
		panic(err)
	}
	log.Print("API started on : " + hostname)
}
