package router

import (
	"github.com/gin-gonic/gin"
	db_handler "gitlab.com/IevgenKabanets/API-Exercise/handlers/pgdb"
)

func ApiEngine() (router *gin.Engine) {

	router = gin.New()
	router.Use(gin.Recovery(), gin.Logger())

	// to stop DEBUG logs, do uncomment a line below
	// gin.SetMode(gin.ReleaseMode)

	// group for /people
	peopleAPI := router.Group("/people")
	//peopleAPI.Use(middleware.Auth(), middleware.CheckStagePermission(), middleware.OrchestraVersion())
	{
		// get all passengers
		peopleAPI.GET("", db_handler.GetAllPassengers)
		// add a passenger
		peopleAPI.POST("", db_handler.AddPassenger)
		// get a passenger
		peopleAPI.GET("/:uuid", db_handler.GetPassenger)
		// delete a passenger
		peopleAPI.DELETE("/:uuid", db_handler.DeletePassenger)
		// update a passenger
		peopleAPI.PUT("/:uuid", db_handler.UpdatePassenger)
	}

	return router
}
