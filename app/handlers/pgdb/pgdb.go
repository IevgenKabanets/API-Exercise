package pgdb

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

type AllPassengers struct {
	Passenger []Passenger
}

type Passenger struct {
	Uuid      uuid.UUID `json:"uuid"`
	Survived  *bool     `json:"survived"`
	Pclass    int       `json:"pclass"`
	Pname     *string   `json:"pname"`
	Sex       string    `json:"sex"`
	Age       float64   `json:"age"`
	SsOnboard int       `json:"ssOnboard"`
	PcOnboard int       `json:"pcOnboard"`
	Fare      float64   `json:"fare"`
}

// establish DB connection or fail
func connectDB() *sql.DB {
	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DATABASE"))
	db, err := sql.Open("postgres", dbinfo)
	checkErr(c, err)
	return db
}

// method to get all of the entries from DB
// db connection is established, query is run and results are put into a struct entity
// nested JSON is created based on structs from step above

func GetAllPassengers(c *gin.Context) {
	var allPassengers AllPassengers
	var passenger Passenger
	db := connectDB()
	defer db.Close()
	fmt.Println("# Querying")
	rows, err := db.Query("SELECT * FROM roster")
	checkErr(c, err)

	for rows.Next() {
		err = rows.Scan(&passenger.Uuid, &passenger.Survived, &passenger.Pclass, &passenger.Pname, &passenger.Sex, &passenger.Age, &passenger.SsOnboard, &passenger.PcOnboard, &passenger.Fare)
		checkErr(c, err)
		allPassengers.Passenger = append(allPassengers.Passenger, passenger)
	}
	c.JSON(200, allPassengers)
}

// method to add a new passenger
// db connection is established, incoming JSON payload is unmarshalled into struct, insert query is run with fields of that struct
// uuid of a new user is returned
func AddPassenger(c *gin.Context) {
	var passenger Passenger
	db := connectDB()
	defer db.Close()
	decoder := json.NewDecoder(c.Request.Body)
	err := decoder.Decode(&passenger)
	checkErr(c, err)

	err = validatePassenger(passenger)
	if err != nil {
		fmt.Println(err.Error())
		c.Writer.Header().Set("Content-Type", "application/json")
		c.JSON(500, err.Error())
		return
	}

	var lastInsertID int
	err = db.QueryRow("INSERT INTO roster(survived,pclass,pname,sex,age,ss_onboard,pc_onboard,fare) VALUES($1,$2,$3,$4,$5,$6,$7,$8) returning uuid;", *passenger.Survived, &passenger.Pclass, *passenger.Pname, &passenger.Sex, &passenger.Age, &passenger.SsOnboard, &passenger.PcOnboard, &passenger.Fare).Scan(&lastInsertID)
	checkErr(c, err)
	fmt.Println("last inserted id =", lastInsertID)
	c.JSON(200, "Added a passenger with uuid: "+strconv.Itoa(lastInsertID))
}

// method to get info of a passenger
// db connection is established, query is run and result is put into struct
// if user is found, struct -> JSON -> response; otherwise - respective message
func GetPassenger(c *gin.Context) {
	var passenger Passenger
	db := connectDB()
	defer db.Close()
	fmt.Println("# Querying")
	err := db.QueryRow("SELECT * FROM roster where uuid=$1", c.Param("uuid")).Scan(&passenger.Uuid, &passenger.Survived, &passenger.Pclass, &passenger.Pname, &passenger.Sex, &passenger.Age, &passenger.SsOnboard, &passenger.PcOnboard, &passenger.Fare)
	switch {
	case err == sql.ErrNoRows:
		c.JSON(200, "No user with uuid "+c.Param("uuid")+" found")
	case err != nil:
		c.JSON(500, err.Error())
	default:
		c.JSON(200, passenger)
	}
}

// method to delete a passenger
// db connection is established, query is run with uuid from request
// based on a result - message is shown to user
func DeletePassenger(c *gin.Context) {
	db := connectDB()
	defer db.Close()
	stmt, err := db.Prepare("delete from roster where uuid=$1")
	checkErr(c, err)

	res, err := stmt.Exec(c.Param("uuid"))
	checkErr(c, err)
	raff, _ := res.RowsAffected()

	if raff > 0 {
		c.JSON(200, "Successfully deleted user with uuid: "+c.Param("uuid"))
	} else {
		c.JSON(200, "There was nothing to delete")
	}

}

// method to delete a passenger
// db connection is established, update query is run with uuid and data from request
// based on a result - message is shown to user

func UpdatePassenger(c *gin.Context) {
	var passenger Passenger
	db := connectDB()
	defer db.Close()
	decoder := json.NewDecoder(c.Request.Body)
	err := decoder.Decode(&passenger)
	checkErr(c, err)

	stmt, err := db.Prepare("update roster set survived=$2, pclass=$3, pname=$4, sex=$5, age=$6, ss_onboard=$7, pc_onboard=$8, fare=$9 where uuid=$1")
	checkErr(c, err)
	res, err := stmt.Exec(c.Param("uuid"), *passenger.Survived, &passenger.Pclass, *passenger.Pname, &passenger.Sex, &passenger.Age, &passenger.SsOnboard, &passenger.PcOnboard, &passenger.Fare)
	checkErr(c, err)

	raff, _ := res.RowsAffected()
	if raff > 0 {
		c.JSON(200, "Successfully updated user with uuid: "+c.Param("uuid"))
	} else {
		c.JSON(200, "There was nothing to update")
	}
}

// supplimentary method to reduce code duplication, can be improved
func checkErr(c *gin.Context, err error) {
	if err != nil {
		fmt.Println("error: " + err.Error())
		c.JSON(500, err.Error())
		return
	}
}

// method to check mandatory fields (well, I found them the very minimal info about passengers fate) presence
func validatePassenger(passenger Passenger) (err error) {
	if passenger.Pname == nil || *passenger.Pname == "" {
		err = errors.New("pname field (passenger name) can't be empty")
		return err
	}
	if passenger.Survived == nil {
		err = errors.New("survived field can't be empty")
		return err
	}
	return nil
}
